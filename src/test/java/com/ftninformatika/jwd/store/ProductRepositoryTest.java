//package com.ftninformatika.jwd.someNewProject;
//
//import com.ftninformatika.jwd.someNewProject.model.Category;
//import com.ftninformatika.jwd.someNewProject.model.Product;
//import com.ftninformatika.jwd.someNewProject.repository.CategoryRepository;
//import com.ftninformatika.jwd.someNewProject.repository.ProductRepository;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
//import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
//import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
//
//import javax.persistence.EntityManager;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Optional;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertTrue;
//
//@DataJpaTest
//@AutoConfigureTestDatabase(replace=Replace.NONE)
//public class ProductRepositoryTest {
//
//
//
//    @Autowired
//    private EntityManager entityManager;
//
//    @Autowired
//    private ProductRepository productRepository;
//    @Autowired
//    private CategoryRepository categoryRepository;
//
//
//    @Test
//
//    public void testFindOneById(){
//        //given
//        Category category=new Category();
//        List<Category> categories=new ArrayList<>();
//        categories.add(category);
//
//        Product product= new Product();
//        product.setName("NewName");
//        product.setPrice(200);
//        product.setStock(20);
//        product.setCategories(categories);
//
//        entityManager.persist(category);
//        entityManager.persist(product);
//        entityManager.flush();
//
//        Long id = product.getId();
//
//        //when
//        Optional<Product> found=productRepository.findOneById(id);
//
//        //then
//        assertTrue(found.isPresent());
//        assertEquals(found.get().getName(), product.getName());
//        assertEquals(found.get().getId(),product.getId());
//        assertEquals(found.get().getPrice(), product.getPrice());
//        assertEquals(found.get().getStock(), product.getStock());
//        assertEquals(found.get().getCategories(), product.getCategories());
//
//
//    }
//
//
//    @Test
//    public void testFindByCategoriesIdAndPriceBetween() {
//        // given
//        Category category = new Category();
//        category.setName("Electronics");
//        List<Category> categories=new ArrayList<>();
//        categories.add(category);
//        entityManager.persist(category);
//        entityManager.flush();
//
//        Product product1 = new Product();
//        product1.setName("Phone");
//        product1.setPrice(500);
//        product1.setStock(10);
//        product1.setCategories(categories);
//        entityManager.persist(product1);
//
//        Product product2 = new Product();
//        product2.setName("Laptop");
//        product2.setPrice(800);
//        product2.setStock(5);
//        product2.setCategories(categories);
//        entityManager.persist(product2);
//
//        Product product3 = new Product();
//        product3.setName("Tablet");
//        product3.setPrice(300);
//        product3.setStock(7);
//        product3.setCategories(categories);
//        entityManager.persist(product3);
//
//        entityManager.flush();
//
//        // when
//        List<Product> found = productRepository.findByCategoriesIdAndPriceBetween(category.getId(), 400, 900);
//
//        // then
//        assertEquals(2, found.size());
//        assertTrue(found.stream().anyMatch(product -> product.getId().equals(product1.getId())));
//        assertTrue(found.stream().anyMatch(product -> product.getId().equals(product2.getId())));
//    }
//
//}
