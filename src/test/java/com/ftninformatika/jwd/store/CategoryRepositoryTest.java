//package com.ftninformatika.jwd.someNewProject;
//
//import static org.assertj.core.api.Assertions.assertThat;
//
//import java.util.Optional;
//
//import com.ftninformatika.jwd.someNewProject.model.Category;
//import com.ftninformatika.jwd.someNewProject.repository.CategoryRepository;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
//import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
//import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
//import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
//
//
//
//import java.util.Optional;
//
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertTrue;
//
//@DataJpaTest
//@AutoConfigureTestDatabase(replace = Replace.NONE)
//public class CategoryRepositoryTest {
//    @Autowired
//    private TestEntityManager entityManager;
//
//    @Autowired
//    private CategoryRepository categoryRepository;
//
//    @Test
//    public void testFindOneById() {
//        // given
//        Category category = new Category();
//        category.setName("Electronics");
//        entityManager.persist(category);
//        entityManager.flush();
//        Long id = category.getId();
//
//        // when
//        Optional<Category> found = categoryRepository.findOneById(id);
//
//        // then
//        assertTrue(found.isPresent());
//        assertEquals(category.getId(), found.get().getId());
//        assertEquals(category.getName(), found.get().getName());
//    }
//
//
//}
