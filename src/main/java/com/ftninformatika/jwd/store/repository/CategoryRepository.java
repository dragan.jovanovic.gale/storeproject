package com.ftninformatika.jwd.store.repository;

import com.ftninformatika.jwd.store.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CategoryRepository extends JpaRepository<Category,Long> {
    Optional<Category> findOneById(Long id);
}
