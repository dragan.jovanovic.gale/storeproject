package com.ftninformatika.jwd.store.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ftninformatika.jwd.store.model.Korisnik;

@Repository
public interface KorisnikRepository extends JpaRepository<Korisnik, Long> {

	Optional<Korisnik> findFirstByKorisnickoIme(String korisnickoIme);

	Optional<Korisnik> findFirstByKorisnickoImeAndLozinka(String korisnickoIme, String lozinka);
}
