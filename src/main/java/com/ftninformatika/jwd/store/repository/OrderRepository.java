package com.ftninformatika.jwd.store.repository;

import com.ftninformatika.jwd.store.model.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<Order,Long> {

    Optional<Order> findOneById(Long id);

    Page<Order> findAll(Pageable pageable);
}
