package com.ftninformatika.jwd.store.repository;

import com.ftninformatika.jwd.store.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product,Long> {
   List <Product> findByCategoriesIdAndPriceBetween(Long categoryId,Integer min,Integer max);

   Optional<Product> findOneById(Long id);

}
