package com.ftninformatika.jwd.store.mapper;

import com.ftninformatika.jwd.store.dto.OrderDto;
import com.ftninformatika.jwd.store.model.Order;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Mapper(componentModel = "spring")
public interface OrderMapper {

    OrderMapper INSTANCE = Mappers.getMapper(OrderMapper.class);


    @Mapping(target = "quantity", source = "quantity")
    @Mapping(target = "productId",source ="product.id")
    OrderDto orderToOrderDto(Order order);


    @Mapping(target = "product.id",source ="productId")
    Order orderDtoToOrder(OrderDto orderDto);

    List<Order> orderDtosToOrders(List<OrderDto> categories);

    List<OrderDto> ordersToOrderDtos(List<Order> categories);
}