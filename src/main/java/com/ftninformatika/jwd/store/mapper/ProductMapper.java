package com.ftninformatika.jwd.store.mapper;

import com.ftninformatika.jwd.store.dto.ProductDto;
import com.ftninformatika.jwd.store.model.Product;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductMapper {

    ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);

    ProductDto productToProductDto(Product product);

    Product productDtoToProduct(ProductDto productDto);

    List<ProductDto>  productsToProducDtos(List<Product> products);

    List<Product> productDtosToProducts(List<ProductDto> productDtos);

}
