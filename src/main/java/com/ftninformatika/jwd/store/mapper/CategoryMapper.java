package com.ftninformatika.jwd.store.mapper;

import com.ftninformatika.jwd.store.dto.CategoryDto;
import com.ftninformatika.jwd.store.model.Category;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Mapper(uses = {OrderMapper.class},componentModel = "spring")
public interface CategoryMapper {
    CategoryMapper INSTANCE = Mappers.getMapper(CategoryMapper.class);

    @IterableMapping(elementTargetType = CategoryDto.class)
    List<CategoryDto> categoriesToCategoryDtos(List<Category> categories);

    @IterableMapping(elementTargetType = Category.class)
    List<Category> categoryDtosToCategories(List<CategoryDto> categoryDtos);

    CategoryDto categoryToCategoryDto(Category category);

    Category categoryDtoToCategory(CategoryDto categoryDto);
}

