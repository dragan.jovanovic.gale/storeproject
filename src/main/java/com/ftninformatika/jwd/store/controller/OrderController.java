package com.ftninformatika.jwd.store.controller;

import com.ftninformatika.jwd.store.dto.OrderDto;
import com.ftninformatika.jwd.store.mapper.OrderMapper;
import com.ftninformatika.jwd.store.model.Order;
import com.ftninformatika.jwd.store.model.Product;
import com.ftninformatika.jwd.store.service.OrderService;
import com.ftninformatika.jwd.store.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.ftninformatika.jwd.store.support.ResponseMessage;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/orders", produces = MediaType.APPLICATION_JSON_VALUE)
public class OrderController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private ProductService productService;



    @Autowired
    private OrderMapper orderMapper;



    @PreAuthorize("hasAnyRole('ADMIN', 'KORISNIK')")
    @GetMapping
    public ResponseEntity<List<OrderDto>> getAll(@RequestParam(value = "pageNo",defaultValue = "0")int pageNo) {
        Page<Order> page = orderService.findPerPage(pageNo);
        HttpHeaders headers= new HttpHeaders();
        headers.add("Total-Pages",Integer.toString(page.getTotalPages()));
        return new ResponseEntity<>(orderMapper.ordersToOrderDtos(page.getContent()), HttpStatus.OK);
    }


    @PreAuthorize("hasAnyRole('ADMIN', 'KORISNIK')")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> create(@Valid @RequestBody OrderDto orderDto) {
        String message = "";
        Order order = orderMapper.orderDtoToOrder(orderDto);

        Optional<Product> optionalProduct = productService.findOneById(orderDto.getProductId());
        if (!optionalProduct.isPresent()) {
            message = "Requested product doesn't exist.";
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseMessage(message));
        }
        Product product = optionalProduct.get();

        if (product.getStock() < order.getQuantity()) {
            message = "Requested quantity not available in stock. Reduce quantity or choose a different item.";
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new ResponseMessage(message));
        }
        product.setStock(product.getStock() - order.getQuantity());

        Order savedOrder = orderService.save(order);
        productService.save(product);


        return new ResponseEntity<>(orderMapper.orderToOrderDto(savedOrder), HttpStatus.CREATED);
    }
}
