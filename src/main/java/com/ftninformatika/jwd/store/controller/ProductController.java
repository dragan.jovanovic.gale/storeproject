package com.ftninformatika.jwd.store.controller;

import com.ftninformatika.jwd.store.dto.ProductDto;
import com.ftninformatika.jwd.store.mapper.ProductMapper;
import com.ftninformatika.jwd.store.model.Product;
import com.ftninformatika.jwd.store.service.CategoryService;
import com.ftninformatika.jwd.store.service.ProductService;
import com.ftninformatika.jwd.store.support.ResponseMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/products",produces = MediaType.APPLICATION_JSON_VALUE)
public class ProductController {

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private CategoryService categoryService;


    @PreAuthorize("hasAnyRole('ADMIN','KORISNIK')")
    @GetMapping
    public ResponseEntity<List<ProductDto>> getAllProducts() {
        List<Product> products= productService.findAll();
        List<ProductDto> productDtos=productMapper.productsToProducDtos(products);
        return new ResponseEntity<>(productDtos=productMapper.productsToProducDtos(products), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('ADMIN','KORISNIK')")
    @GetMapping("/{id}")
    public  ResponseEntity<?> getProduct(@PathVariable Long id) {
        String message = "";
        Optional<Product> productOptional = productService.findOneById(id);
        if(!productOptional.isPresent()){
            message = "Requested product doesn't exist";
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseMessage(message));
        } else{
            ProductDto productDto=productMapper.productToProductDto(productOptional.get());
            return new ResponseEntity<>(productDto, HttpStatus.OK);
        }
    }


    @PreAuthorize("hasAnyRole('ADMIN', 'KORISNIK')")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProductDto> create(@Valid @RequestBody ProductDto productDto) {
        String message = "";
        Product product = productMapper.productDtoToProduct(productDto);

        Product savedOrder = productService.save(product);


        return new ResponseEntity<>(productMapper.productToProductDto(product), HttpStatus.CREATED);
    }


}
