package com.ftninformatika.jwd.store.impl;

import com.ftninformatika.jwd.store.model.Category;
import com.ftninformatika.jwd.store.repository.CategoryRepository;
import com.ftninformatika.jwd.store.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;
    @Override
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Optional<Category> findOneById(Long id) {
        return categoryRepository.findOneById(id);
    }
}
