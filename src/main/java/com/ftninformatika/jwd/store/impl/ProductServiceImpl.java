package com.ftninformatika.jwd.store.impl;

import com.ftninformatika.jwd.store.model.Product;
import com.ftninformatika.jwd.store.repository.CategoryRepository;
import com.ftninformatika.jwd.store.repository.ProductRepository;
import com.ftninformatika.jwd.store.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public List<Product> findAll() {
        return productRepository.findAll();
    }

    @Override
    public Optional<Product> findOneById(Long id) {
        return productRepository.findOneById(id);
    }

    @Override
    public Product save(Product product) {
        return productRepository.save(product);
    }

    @Override
    public void delete(Long id) {
        Optional product=Optional.of(productRepository.findOneById(id));
        if(product.isPresent()){
            productRepository.deleteById(id);
        }

    }

    @Override
    public Product update(Product product) {
        return productRepository.save(product);
    }

    @Override
    public List<Product> findBetweenMinAndMaxAndCategoriesId(Integer min, Integer max, Long categoryId) {
        return productRepository.findByCategoriesIdAndPriceBetween(categoryId,min,max);
    }
}
