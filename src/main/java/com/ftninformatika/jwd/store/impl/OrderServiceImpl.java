package com.ftninformatika.jwd.store.impl;

import com.ftninformatika.jwd.store.model.Order;
import com.ftninformatika.jwd.store.repository.OrderRepository;
import com.ftninformatika.jwd.store.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;
    @Override
    public List<Order> findAll() {
        return orderRepository.findAll();
    }

    @Override
    public Optional<Order> findOneById(Long id) {
        return orderRepository.findOneById(id);
    }

    @Override
    public Order save(Order order) {
        return orderRepository.save(order);
    }

    @Override
    public Page findPerPage(Integer pageNo) {
        return orderRepository.findAll(PageRequest.of(pageNo,2));
    }
}
