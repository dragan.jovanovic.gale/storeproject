package com.ftninformatika.jwd.store.service;

import com.ftninformatika.jwd.store.model.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface OrderService {

    List<Order> findAll();

    Optional<Order> findOneById(Long id);

    Order save(Order order);

    Page findPerPage(Integer pageNo);
}
