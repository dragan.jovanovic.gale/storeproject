package com.ftninformatika.jwd.store.service;

import com.ftninformatika.jwd.store.model.Category;

import java.util.List;
import java.util.Optional;

public interface CategoryService {

    List<Category> findAll();

    Optional<Category> findOneById(Long id);

}
