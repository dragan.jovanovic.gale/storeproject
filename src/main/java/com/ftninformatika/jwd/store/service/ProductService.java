package com.ftninformatika.jwd.store.service;

import com.ftninformatika.jwd.store.model.Product;

import java.util.List;
import java.util.Optional;

public interface ProductService {

    List<Product> findAll();

    Optional<Product> findOneById(Long id);

    Product save(Product product);

    void delete(Long id);

    Product update(Product product);

    List<Product> findBetweenMinAndMaxAndCategoriesId(Integer min,Integer max,Long categoryId);

}
