package com.ftninformatika.jwd.store.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.store.dto.KorisnikDTO;
import com.ftninformatika.jwd.store.model.Korisnik;
import com.ftninformatika.jwd.store.service.KorisnikService;

@Component
public class KorisnikDtoToKorisnik implements Converter<KorisnikDTO, Korisnik> {

	@Autowired
	private KorisnikService korisnikService;

	@Override
	public Korisnik convert(KorisnikDTO korisnikDTO) {
		Korisnik korisnik = null;
		if (korisnikDTO.getId() != null) {
			korisnik = korisnikService.findOne(korisnikDTO.getId()).get();
		}

		if (korisnik == null) {
			korisnik = new Korisnik();
		}

		korisnik.setKorisnickoIme(korisnikDTO.getKorisnickoIme());
		korisnik.seteMail(korisnikDTO.geteMail());
		return korisnik;
	}

}
