package com.ftninformatika.jwd.store.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.store.dto.KorisnikDTO;
import com.ftninformatika.jwd.store.model.Korisnik;

@Component
public class KorisnikToKorisnikDto implements Converter<Korisnik, KorisnikDTO> {

	@Override
	public KorisnikDTO convert(Korisnik korisnik) {
		KorisnikDTO korisnikDTO = new KorisnikDTO();

		korisnikDTO.setId(korisnik.getId());
		korisnikDTO.seteMail(korisnik.geteMail());
		korisnikDTO.setKorisnickoIme(korisnik.getKorisnickoIme());

		return korisnikDTO;
	}

	public List<KorisnikDTO> convert(List<Korisnik> korisnici) {
		List<KorisnikDTO> korisnikDTOS = new ArrayList<>();

		for (Korisnik k : korisnici) {
			KorisnikDTO dto = convert(k);
			korisnikDTOS.add(dto);
		}

		return korisnikDTOS;
	}
}
