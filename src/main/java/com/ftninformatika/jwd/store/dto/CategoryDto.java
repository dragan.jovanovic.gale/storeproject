package com.ftninformatika.jwd.store.dto;

import com.ftninformatika.jwd.store.model.Product;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoryDto {
    private Long id;


    private String name;


    private Product product;
}