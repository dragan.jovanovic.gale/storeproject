package com.ftninformatika.jwd.store.dto;

import org.springframework.lang.Nullable;

import javax.validation.constraints.NotNull;

public class ProductDto {

    private Long id;
    @NotNull
    private String name;

    private int  price;

    private int stock;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }
}
