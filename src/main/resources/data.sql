INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, uloga)
              VALUES (1,'miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','ADMIN');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, uloga)
              VALUES (2,'tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, uloga)
              VALUES (3,'petar@maildrop.cc','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, uloga)
              VALUES (4,'mare@gmail.com','mare','$2a$10$hW5o6Trv06hWFfqfm2zN9.foYgCIcxwVl7wGWUo0QMdiCLOsIpnk2','ADMIN');


INSERT INTO product(id,name,price,stock)
			VALUES(1,"Cips",75,65);

INSERT INTO product(id,name,price,stock)
			VALUES(2,"Jogurt",35,10);

INSERT INTO product(id,name,price,stock)
			VALUES(3,"Smoki",55,101);


INSERT INTO category(id,name)
			VALUES(1,"Grickalice");

INSERT INTO category(id,name)
			VALUES(2,"Mlecni Proizvodi");

INSERT INTO category(id,name)
			VALUES(3,"Pica");

INSERT INTO category_product(category_id,product_id) VALUES(2,2);

INSERT INTO category_product(category_id,product_id) VALUES(3,2);

INSERT INTO category_product(category_id,product_id) VALUES(1,1);

INSERT INTO category_product(category_id,product_id) VALUES(1,3);

INSERT INTO orders(id,quantity,product_id) VALUES(1,3,3);
INSERT INTO orders(id,quantity,product_id) VALUES(2,8,1);
INSERT INTO orders(id,quantity,product_id) VALUES(3,6,2);
